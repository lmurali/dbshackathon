package org.hackathon.PageComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.hackathon.AbstractComponents.AbstractComponent;

public class footerNavigation extends AbstractComponent {
    WebDriver driver;
    By sectionElement = By.id("traveller-home"); //Look for elements within the footer alone
    By flights = By.cssSelector("[title='Flights']"); //Look for Flights link

    public footerNavigation(WebDriver driver, By sectionElement) {
        super(driver, sectionElement); //when you inherit parent class you should invoke parent class constructor in your own child class
        // constructor
    }
    //method to handle flights
    // when any method executes any method in this class  - scope of selenium should be in footer only

    public void selectFlights ()
    {
        //customized findelement
        findElement(flights).click();
            //driver.findElement(flights).click();
    }

    public void getFlightAttribute ()
    {
        //customized findelement
        //findElement(flights).click();
        System.out.println("Get property : " + findElement(flights).getAttribute("class") );
        //driver.findElement(flights).click();
    }

}
