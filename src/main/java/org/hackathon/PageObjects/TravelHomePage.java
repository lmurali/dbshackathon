package org.hackathon.PageObjects;

// URL: https://www.rahulshettyacademy.com/dropdownsPractise/#

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.hackathon.PageComponents.footerNavigation;
import org.hackathon.PageComponents.navigationBar;

public class TravelHomePage {
    By sectionElement = By.id("traveller-home");
    WebDriver driver;

    public TravelHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void goTo()
    {
        driver.get("https://www.rahulshettyacademy.com/dropdownsPractise/");
    }

    public navigationBar getNavigationBar ()
    {
        return new navigationBar();
    }
    
    public footerNavigation getFooterNavigation ()
    {
        return new footerNavigation(driver, sectionElement);
    }

}
