package org.hackathon;

import static org.junit.Assert.assertTrue;

import org.hackathon.PageObjects.TravelHomePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for simple App.
 */
public class FirstTestCase
{
    /**
     * Rigorous Test :-)
     */

    @BeforeMethod
    void beforeMethod()
    {
        System.out.println("*********** Start of a new test case *************");
    }
    @Test
    public void flightTest()
    {
        System.setProperty("webdriver.chrome.driver","C:\\webdriver\\chromedriver 99\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        TravelHomePage travelHomePage = new TravelHomePage(driver);
        travelHomePage.goTo();
        travelHomePage.getFooterNavigation().getFlightAttribute();
    }

    @Test (priority = 1)
    public void loginTestCase()
    {
        System.out.println("Inside Login");
        Assert.fail("Failed Login");
    }


    @AfterMethod
    void afterMethod()
    {
        System.out.println("********** End of test case ************");
    }

    public void teardown()
    {
        System.out.println("Closing the browser");

    }
}
