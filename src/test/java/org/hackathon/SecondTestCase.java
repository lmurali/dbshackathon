package org.hackathon;

import org.hackathon.PageObjects.TravelHomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SecondTestCase {
    @BeforeTest
    void beforeTest()
    {
        System.out.println("*********** @BeforeTest Start of a new test case *************");
    }
    @Test
    public void secondTest()
    {
        System.out.println("Second Test Case - Its in a new branch now");
    }

    @AfterTest
    void afterTest()
    {
        System.out.println("*********** @AfterTest End of a test case *************");
    }

}
